package ua.tolcom.delivery.consts;

/**
 * Created by Andoliny on 09.10.2016.
 */

public final class DeliveryConsts {
    public static final String PREFS = "Preferences";
    public static final String PREF_SERVER_ADDR = "ServerAddress";
    public static final String PREF_MESSAGE_TEXT = "MessageText";
    public static final String PREF_INTERVAL = "Interval";

    /*public final static String API_ENDPOINT = "http://176.37.40.122:9009/";*/
    //public final static String API_ENDPOINT = "http://95.67.37.195:17000/";
   // public final static String API_ENDPOINT = "http://192.168.0.117:8778/";

    public static final String DEFAULT_MESSAGE = "Здравствуйте, {userName}. Номер вашего заказа {orderId}. Спасибо, что покупаете с Tolkom!";
    public static final String DEFAULT_ADDRESS = "http://192.168.0.117:8778";
    public static final Integer DEFAULT_INTERVAL_MINUTES = 1;

    public final static String BROADCAST_MAINSERVICE = "ua.tolcom.smsdelivery.foreground.main";
    public final static String BROADCAST_WORKSERVICE = "ua.tolcom.smsdelivery.background.process";
    public final static String TAG_LOG = "myLog";

    public final static int STATUS_START = 100;
    public final static int STATUS_FINISH = 200;
    public final static int STATUS_START_DOWNLOAD = 300;
    public final static int STATUS_FINISH_DOWNLOAD = 400;
    public final static int STATUS_SERVER_QUERY_ERROR = -300;

    public final static int STATUS_START_SENDING = 500;
    public final static int STATUS_FINISH_SENDING = 600;

    public final static int STATE_NEW = 1;
    public final static int STATE_IN_PROCESS = 2;
    public final static int STATE_COMPLETE = 3;

    public final static int DOWNLOADING_TASK = 1;
    public final static int SENDING_TASK = 2;

    public final static String PARAM_TASK = "task";
    public final static String PARAM_STATUS = "status";
    public final static String PARAM_DOWNLOAD_QUANTITY = "downloadQuantity";
    public final static String PARAM_SENT_QUANTITY = "sentQuantity";
    public final static String PARAM_ERROR= "paramError";

    public final static long INTERVAL_MULTIPLIER = 1; // 1 min
    public final static long FIRST_RUN = 0L; // 1 sec
    public final static int REQUEST_CODE = 777888;
}
