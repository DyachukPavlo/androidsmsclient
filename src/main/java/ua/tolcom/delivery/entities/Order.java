package ua.tolcom.delivery.entities;

/**
 * Created by Andoliny on 03.11.2016.
 */

import android.net.Uri;

import static ua.tolcom.delivery.database.DataProviderContract.CONTENT_URI;

public class Order implements DataStruct {

    public static final String ORDER_TABLE_NAME = "orders";
    public static final Uri ORDER_TABLE_CONTENTURI =
            Uri.withAppendedPath(CONTENT_URI, ORDER_TABLE_NAME);

    public static final String ORDER_ID = "_id";
    public static final String ORDER_ID_ORDER= "id_order";
    public static final String ORDER_NAME = "name";
    public static final String ORDER_PRICE = "price";
    public static final String ORDER_TOTAL_PRICE = "total_price";
    public static final String ORDER_QUANTITY = "quantity";
    public static final String ORDER_CLIENT_ID= "id_client";
    public static final String ORDER_CLIENT_NAME= "client_name";
    public static final String ORDER_CLIENT_PHONE = "phone";
    public static final String ORDER_STATE = "id_state";
    public static final String ORDER_DATE = "orderDate";
    public static final String ORDER_DELIVERY_DATE = "deliveryDate";

    private final String[] ORDER_TABLE_COLUMNS = {ORDER_ID, ORDER_ID_ORDER, ORDER_NAME, ORDER_PRICE, ORDER_TOTAL_PRICE, ORDER_QUANTITY,
                                                  ORDER_STATE, ORDER_CLIENT_ID, ORDER_CLIENT_NAME, ORDER_CLIENT_PHONE, ORDER_DATE, ORDER_DELIVERY_DATE};


    private static int idOrder;
    private String nameOrder;
    private float price;
    private float totalPrice;
    private int qountity;
    private int state;
    private int idClient;
    private String nameClient;
    private String phone;
    private String orderDate;
    private String deliveryDate;

    public Order() {
    }

    public Order(int idOrder, String nameOrder, float price, float totalPrice, int qountity, int state, int idClient, String nameClient, String phone, String orderDate, String deliveryDate) {
        this.idOrder = idOrder;
        this.nameOrder = nameOrder;
        this.price = price;
        this.totalPrice = totalPrice;
        this.qountity = qountity;
        this.state = state;
        this.idClient = idClient;
        this.nameClient = nameClient;
        this.phone = phone;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "idOrder=" + idOrder +
                ", nameOrder='" + nameOrder + '\'' +
                ", price=" + price +
                ", totalPrice=" + totalPrice +
                ", qountity=" + qountity +
                ", state=" + state +
                ", idClient=" + idClient +
                ", nameClient='" + nameClient + '\'' +
                ", phone='" + phone + '\'' +
                ", dateOrder='" + orderDate + '\'' +
                ", deliveryDate='" + deliveryDate + '\'' +
                '}';
    }

    public int getIdOrder() {
        return idOrder;
    }

    public static void setIdOrder(int idOrder) {
        Order.idOrder = idOrder;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public void setNameOrder(String nameOrder) {
        this.nameOrder = nameOrder;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getQountity() {
        return qountity;
    }

    public void setQountity(int qountity) {
        this.qountity = qountity;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String[] getORDER_TABLE_COLUMNS() {
        return ORDER_TABLE_COLUMNS;
    }

    @Override
    public String[] getColumns() {
        return getORDER_TABLE_COLUMNS();
    }
}
