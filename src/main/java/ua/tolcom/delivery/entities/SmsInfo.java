package ua.tolcom.delivery.entities;

/**
 * Created by Andoliny on 19.09.2016.
 */
public class SmsInfo {

    private Integer idOrder;
    private String orderDate;
    private String deliveryDate;
    private String clientName;
    private String productName;
    private Double price;
    private Integer count;
    private Double totalPrice;
    private String phoneNamber;

    public SmsInfo() {
    }

    public SmsInfo(Integer idOrder, String orderDate, String deliveryDate, String clientName, String productName, Double price, Integer count, Double totalPrice, String phoneNamber) {
        this.idOrder = idOrder;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
        this.clientName = clientName;
        this.productName = productName;
        this.price = price;
        this.count = count;
        this.totalPrice = totalPrice;
        this.phoneNamber = phoneNamber;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getPhoneNamber() {
        return phoneNamber;
    }

    public void setPhoneNamber(String phoneNamber) {
        this.phoneNamber = phoneNamber;
    }

    @Override
    public String toString() {
        return "SmsInfo{" +
                "idOrder=" + idOrder +
                ", orderDate='" + orderDate + '\'' +
                ", deliveryDate='" + deliveryDate + '\'' +
                ", clientName='" + clientName + '\'' +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", totalPrice=" + totalPrice +
                ", phoneNamber='" + phoneNamber + '\'' +
                '}';
    }
}

