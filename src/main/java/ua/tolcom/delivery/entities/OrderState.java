package ua.tolcom.delivery.entities;

import android.net.Uri;

import static ua.tolcom.delivery.database.DataProviderContract.*;

/**
 * Created by Andoliny on 03.11.2016.
 */

public class OrderState implements DataStruct{

    public static final String STATE_TABLE_NAME = "order_state";
    public static final Uri ORDER_STATE_TABLE_CONTENTURI =
            Uri.withAppendedPath(CONTENT_URI, STATE_TABLE_NAME);
    public static final String STATE_ID = "_id";
    public static final String STATE_NAME_STATE = "state";

    public static final String[] STATE_TABLE_COLUMNS = {STATE_ID, STATE_NAME_STATE};


    private int idState;
    private String state;

    public OrderState() {
    }

    public OrderState(int idState, String state) {
        this.idState = idState;
        this.state = state;
    }

    @Override
    public String toString() {
        return "OrderState{" +
                "idState=" + idState +
                ", state='" + state + '\'' +
                '}';
    }


    public int getIdState() {
        return idState;
    }

    public String getState() {
        return state;
    }

    private String[] getOrderStateTableColumns() {
        return STATE_TABLE_COLUMNS;
    }

    @Override
    public String[] getColumns() {
        return getOrderStateTableColumns();
    }
}
