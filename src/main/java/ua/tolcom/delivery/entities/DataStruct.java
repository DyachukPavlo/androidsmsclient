package ua.tolcom.delivery.entities;

/**
 * Created by Andoliny on 04.11.2016.
 */

public interface DataStruct {
    public String[] getColumns();
}
