package ua.tolcom.delivery.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import java.util.HashMap;

import ua.tolcom.delivery.entities.Order;

import static ua.tolcom.delivery.consts.DeliveryConsts.*;
import static ua.tolcom.delivery.database.DataProviderContract.*;
import static ua.tolcom.delivery.utils.DataUtils.*;
import static ua.tolcom.delivery.entities.Order.*;
import static ua.tolcom.delivery.entities.OrderState.*;

/**
 * Created by Andoliny on 10.10.2016.
 */

public class DataProvider extends ContentProvider {


    // Создание таблицы Order
    public static final String CREATE_TABLE_ORDER = "CREATE TABLE" + " " +
            ORDER_TABLE_NAME + " " +
            "(" + " " +
            ORDER_ID + " " + PRIMARY_KEY_TYPE + " ," +
            ORDER_ID_ORDER + " " + INTEGER_TYPE + " ," +
            ORDER_NAME+ " " + TEXT_TYPE + " ," +
            ORDER_PRICE + " " + FLOAT_TYPE + " ," +
            ORDER_TOTAL_PRICE+ " " + FLOAT_TYPE + " ," +
            ORDER_QUANTITY + " " + INTEGER_TYPE + " ,"+
            ORDER_STATE + " " + INTEGER_TYPE + " ,"+
            ORDER_CLIENT_ID + " " + INTEGER_TYPE + " ,"+
            ORDER_CLIENT_NAME + " " + TEXT_TYPE + " ,"+
            ORDER_CLIENT_PHONE + " " + TEXT_TYPE + " ,"+
            ORDER_DATE + " " + TEXT_TYPE + " ,"+
            ORDER_DELIVERY_DATE + " " + TEXT_TYPE +
            ")";

    // Создание таблицы Order_State
    public static final String CREATE_TABLE_ORDER_STATE = "CREATE TABLE" + " " +
            STATE_TABLE_NAME + " " +
            "(" + " " +
            STATE_ID + " " + PRIMARY_KEY_TYPE + " ," +
            STATE_NAME_STATE + " " + INTEGER_TYPE +
            ")";


    //// UriMatcher
    // общий Uri

    private SQLiteOpenHelper openHelper;

    private static final UriMatcher uriMatcher;

    private static final SparseArray<String> mimeTypes;
    private static final HashMap<String, String> columnMap = buildColumnMap();


    static {

        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mimeTypes = new SparseArray<String>();

        uriMatcher.addURI(
                AUTHORITY,
                ORDER_TABLE_NAME,
                URI_ORDER_QUERY);
        uriMatcher.addURI(
                AUTHORITY,
                ORDER_TABLE_NAME + BY_CLIENT + "#",
                URI_ORDER_BY_CLIENT_QUERY);
        uriMatcher.addURI(
                AUTHORITY,
                ORDER_TABLE_NAME + BY_ID + "#",
                URI_ORDER_BY_ID_QUERY);
        uriMatcher.addURI(
                AUTHORITY,
                ORDER_TABLE_NAME + BY_STATE + "#",
                URI_ORDER_BY_STATE_QUERY);
        uriMatcher.addURI(
                AUTHORITY,
                ORDER_TABLE_NAME + ID_IN + "*",
                URI_ORDER_IN_ID_QUERY);
        uriMatcher.addURI(
                AUTHORITY,
                STATE_TABLE_NAME,
                URI_STATE_QUERY);
        uriMatcher.addURI(
                AUTHORITY,
                STATE_TABLE_NAME + BY_ID + "#",
                URI_STATE_BYID_QUERY);
        //захешируем мимитипы
        mimeTypes.put(
                URI_ORDER_QUERY,
                MIME_TYPE_ROWS + "." +
                        ORDER_TABLE_NAME);
        mimeTypes.put(
                URI_ORDER_BY_CLIENT_QUERY,
                MIME_TYPE_ROWS + "." +
                        ORDER_TABLE_NAME);
        mimeTypes.put(
                URI_ORDER_IN_ID_QUERY,
                MIME_TYPE_ROWS + "." +
                        ORDER_TABLE_NAME);
        mimeTypes.put(
                URI_ORDER_BY_ID_QUERY,
                MIME_TYPE_SINGLE_ROW + "." +
                        ORDER_TABLE_NAME);
        mimeTypes.put(
                URI_STATE_QUERY,
                MIME_TYPE_ROWS + "." +
                        STATE_TABLE_NAME);
        mimeTypes.put(
                URI_STATE_BYID_QUERY,
                MIME_TYPE_SINGLE_ROW + "." +
                        STATE_TABLE_NAME);
    }


    public void close() {
        openHelper.close();
    }


    private class DataProviderHelper extends SQLiteOpenHelper {

        DataProviderHelper(Context context) {
            super(context,
                    DATABASE_NAME,
                    null,
                    DATABASE_VERSION);
        }

        private void dropTables(SQLiteDatabase db) {
            db.execSQL("DROP TABLE IF EXISTS " + ORDER_TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + STATE_TABLE_NAME);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_ORDER);
            Log.w(DataProviderHelper.class.getName(), CREATE_TABLE_ORDER);
            db.execSQL(CREATE_TABLE_ORDER_STATE);
            Log.w(DataProviderHelper.class.getName(), CREATE_TABLE_ORDER_STATE);

            fillData(db);
        }

        private void fillData(SQLiteDatabase db){
            if( db != null ){
                ContentValues values;
                values = new ContentValues();
                values.put(STATE_NAME_STATE, STATE_NEW);
                db.insert(STATE_TABLE_NAME, null, values);

                values = new ContentValues();
                values.put(STATE_NAME_STATE, STATE_IN_PROCESS);
                db.insert(STATE_TABLE_NAME, null, values);

                values = new ContentValues();
                values.put(STATE_NAME_STATE, STATE_COMPLETE);
                db.insert(STATE_TABLE_NAME, null, values);

                Log.d(TAG_LOG,"STATE_TABLE is created");
            }
            else {
                Log.d(TAG_LOG,"db is not exists");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int version1, int version2) {
            Log.w(DataProviderHelper.class.getName(),
                    "Upgrading database from version " + version1 + " to "
                            + version2 + ", which will destroy all the existing data");
            dropTables(db);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int version1, int version2) {
            Log.w(DataProviderHelper.class.getName(),
                    "Downgrading database from version " + version1 + " to "
                            + version2 + ", which will destroy all the existing data");
            dropTables(db);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {

        openHelper = new DataProviderHelper(getContext());
        Log.d(TAG_LOG, "db is created");
        return true;
    }

    private static HashMap<String, String> buildColumnMap() {
        HashMap<String, String> map = new HashMap<String, String>();

        String orderProjection[] = getTableColumns(new Order());
        for (String col : orderProjection) {

            String qualifiedCol = addPrefix(ORDER_TABLE_NAME , col);
            map.put(qualifiedCol, qualifiedCol + " as " + col);
        }

        String stateProjection[] = STATE_TABLE_COLUMNS;
        for (String col : stateProjection) {

            String qualifiedCol = addPrefix(STATE_TABLE_NAME, col);
            String alias = qualifiedCol.replace(".", ALIAS_JOINER);
            map.put(qualifiedCol, qualifiedCol + " AS " + alias);
        }

        return map;
    }

    // чтение
   /* public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.d(TAG_LOG, "query, " + uri.toString());

        SQLiteDatabase db = openHelper.getReadableDatabase();
        Cursor returnCursor = null;
        switch (uriMatcher.match(uri)) {

            case URI_ORDER_QUERY:
                returnCursor = db.query(
                        ORDER_TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_ORDER_BY_ID_QUERY:
                String idOrder = uri.getLastPathSegment();
                Log.d(TAG_LOG, "URI_ORDER_BY_ID_QUERY, " + idOrder);
                if (TextUtils.isEmpty(selection)){
                    selection = ORDER_ID +" = " + idOrder;
                }
                else{
                    selection = selection + " AND " + ORDER_ID +" = " + idOrder;
                }
                returnCursor = db.query(
                        ORDER_TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                // No notification Uri is set, because the data doesn't have to be watched.
                return returnCursor;

            case URI_ORDER_BY_CLIENT_QUERY:
                String idClientOrder = uri.getLastPathSegment();
                Log.d(TAG_LOG, "URI_ORDER_BY_CLIENT_QUERY, " + idClientOrder);
                if (TextUtils.isEmpty(selection)){
                    selection = ORDER_CLIENT_ID +" = " + idClientOrder;
                }
                else{
                    selection = selection + " AND " + ORDER_CLIENT_ID +" = " + idClientOrder;
                }
                    returnCursor = db.query(
                        ORDER_TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_STATE_QUERY:
                Log.d(TAG_LOG, "URI_STATE_QUERY ");

                returnCursor = db.query(
                        STATE_TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_STATE_BYID_QUERY:
                String idOrderState = uri.getLastPathSegment();
                Log.d(TAG_LOG, "URI_STATE_BYID_QUERY, " + idOrderState);
                if (TextUtils.isEmpty(selection)){
                    selection = STATE_ID +" = " + idOrderState;
                }
                else{
                    selection = selection + " AND " + STATE_ID + " = " + idOrderState;
                }
                returnCursor = db.query(
                        STATE_TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;


            case INVALID_URI:

                throw new IllegalArgumentException("Query -- Invalid URI:" + uri);
        }

        return returnCursor;
    }*/

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        String table;
        StringBuilder sb = new StringBuilder();

        Log.d(TAG_LOG, "query, " + uri.toString());

        Log.d(TAG_LOG, "PATH= " + uri.getPath());

        Cursor returnCursor = null;
        queryBuilder.setProjectionMap(columnMap);

        switch (uriMatcher.match(uri)) {

            case URI_ORDER_QUERY:
                sb.append(ORDER_TABLE_NAME);
                sb.append(" LEFT OUTER JOIN ");
                sb.append(STATE_TABLE_NAME);
                sb.append(" ON (");
                sb.append(ORDER_STATE);
                sb.append(" = ");
                sb.append(addPrefix(STATE_TABLE_NAME, STATE_ID));
                sb.append(")");
                table = sb.toString();
                queryBuilder.setTables(table);

                Log.d(TAG_LOG, "selct: URI_ORDER_QUERY ");

                returnCursor = queryBuilder.query(openHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_ORDER_BY_ID_QUERY:
                sb.append(ORDER_TABLE_NAME);
                sb.append(" LEFT OUTER JOIN ");
                sb.append(STATE_TABLE_NAME);
                sb.append(" ON (");
                sb.append(ORDER_STATE);
                sb.append(" = ");
                sb.append(addPrefix(STATE_TABLE_NAME, STATE_ID));
                sb.append(")");
                table = sb.toString();
                queryBuilder.setTables(table);
                String idOrder = uri.getLastPathSegment();

                Log.d(TAG_LOG, "select: URI_ORDER_BY_ID_QUERY, " + idOrder);

                if (TextUtils.isEmpty(selection)){
                    selection = ORDER_ID +" = " + idOrder;
                }
                else{
                    selection = selection + " AND " + ORDER_ID +" = " + idOrder;
                }
                returnCursor = queryBuilder.query(openHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_ORDER_BY_CLIENT_QUERY:
                sb.append(ORDER_TABLE_NAME);
                sb.append(" LEFT OUTER JOIN ");
                sb.append(STATE_TABLE_NAME);
                sb.append(" ON (");
                sb.append(ORDER_STATE);
                sb.append(" = ");
                sb.append(addPrefix(STATE_TABLE_NAME, STATE_ID));
                sb.append(")");
                table = sb.toString();
                queryBuilder.setTables(table);
                String idClientOrder = uri.getLastPathSegment();

                Log.d(TAG_LOG, "select: URI_ORDER_BY_CLIENT_QUERY, " + idClientOrder);

                if (TextUtils.isEmpty(selection)){
                    selection = ORDER_CLIENT_ID +" = " + idClientOrder;
                }
                else{
                    selection = selection + " AND " + ORDER_CLIENT_ID +" = " + idClientOrder;
                }
                returnCursor = queryBuilder.query(openHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_ORDER_IN_ID_QUERY:
                sb.append(ORDER_TABLE_NAME);
                sb.append(" LEFT OUTER JOIN ");
                sb.append(STATE_TABLE_NAME);
                sb.append(" ON (");
                sb.append(ORDER_STATE);
                sb.append(" = ");
                sb.append(addPrefix(STATE_TABLE_NAME, STATE_ID));
                sb.append(")");
                table = sb.toString();
                queryBuilder.setTables(table);
                String listId = uri.getLastPathSegment();

                Log.d(TAG_LOG, "select: URI_ORDER_IN_ID_QUERY, " + listId);

                if (TextUtils.isEmpty(selection)){
                    selection = ORDER_ID_ORDER +" in (" + listId + ")";
                }
                else{
                    selection = selection + " AND " + ORDER_ID_ORDER +" in (" + listId + ")";
                }
                returnCursor = queryBuilder.query(openHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_ORDER_BY_STATE_QUERY:
                sb.append(ORDER_TABLE_NAME);
                sb.append(" LEFT OUTER JOIN ");
                sb.append(STATE_TABLE_NAME);
                sb.append(" ON (");
                sb.append(ORDER_STATE);
                sb.append(" = ");
                sb.append(addPrefix(STATE_TABLE_NAME, STATE_ID));
                sb.append(")");
                table = sb.toString();
                queryBuilder.setTables(table);
                String idState = uri.getLastPathSegment();

                Log.d(TAG_LOG, "select: URI_ORDER_BY_ID_QUERY, " + idState);

                if (TextUtils.isEmpty(selection)){
                    selection = ORDER_STATE +" = " + idState;
                }
                else{
                    selection = selection + " AND " + ORDER_STATE +" = " + idState;
                }
                returnCursor = queryBuilder.query(openHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_STATE_QUERY:
                sb.append(STATE_TABLE_NAME);
                table = sb.toString();
                queryBuilder.setTables(table);

                Log.d(TAG_LOG, "select: URI_STATE_QUERY ");

                returnCursor = queryBuilder.query(openHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case URI_STATE_BYID_QUERY:
                sb.append(STATE_TABLE_NAME);
                table = sb.toString();
                queryBuilder.setTables(table);
                String idOrderState = uri.getLastPathSegment();

                Log.d(TAG_LOG, "select: URI_STATE_BYID_QUERY, " + idOrderState);

                if (TextUtils.isEmpty(selection)){
                    selection = STATE_ID +" = " + idOrderState;
                }
                else{
                    selection = selection + " AND " + STATE_ID + " = " + idOrderState;
                }
                returnCursor = queryBuilder.query(openHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;


            case INVALID_URI:

                throw new IllegalArgumentException("Query -- Invalid URI:" + uri);
        }

        return returnCursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase localSQLiteDatabase;
        long id;
        // Decode the URI to choose which action to take
        switch (uriMatcher.match(uri)) {

            case URI_ORDER_QUERY:

                // Creates a writeable database or gets one from cache
                localSQLiteDatabase = openHelper.getWritableDatabase();

                // Inserts the row into the table and returns the new row's _id value
                id = localSQLiteDatabase.insert(
                        ORDER_TABLE_NAME,
                        null,
                        values
                );

                // If the insert succeeded, notify a change and return the new row's content URI.
                if (-1 != id) {
                    getContext().getContentResolver().notifyChange(uri, null);
                    return Uri.withAppendedPath(uri, Long.toString(id));
                } else {

                    throw new SQLiteException("Insert error:" + uri);
                }

            case URI_STATE_QUERY:

                // Creates a writeable database or gets one from cache
                localSQLiteDatabase = openHelper.getWritableDatabase();

                // Inserts the row into the table and returns the new row's _id value
                id = localSQLiteDatabase.insert(
                        STATE_TABLE_NAME,
                        null,
                        values
                );

                // If the insert succeeded, notify a change and return the new row's content URI.
                if (-1 != id) {
                    getContext().getContentResolver().notifyChange(uri, null);
                    return Uri.withAppendedPath(uri, Long.toString(id));
                } else {

                    throw new SQLiteException("Insert error:" + uri);
                }
        }

        return null;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d(TAG_LOG, "delete, " + uri.toString());
        throw new UnsupportedOperationException("Delete -- unsupported operation " + uri);
    }

    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase localSQLiteDatabase;

        Log.d(TAG_LOG, "update, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ORDER_QUERY:
                Log.d(TAG_LOG, "update: URI_ORDER_QUERY");
                break;
            case URI_ORDER_BY_ID_QUERY:
                String id = uri.getLastPathSegment();
                Log.d(TAG_LOG, "update: URI_ORDER_BY_ID_QUERY, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = ORDER_ID_ORDER + " = " + id;
                } else {
                    selection = selection + " AND " + ORDER_ID_ORDER + " = " + id;
                }
                break;
            case URI_ORDER_IN_ID_QUERY:
                Log.d(TAG_LOG, "update: URI_ORDER_IN_ID_QUERY, ");
                String listId = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = ORDER_ID_ORDER + " in( " + listId + ")";
                } else {
                    selection = selection + " AND " + ORDER_ID_ORDER + " in( " + listId + ")";
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        localSQLiteDatabase = openHelper.getWritableDatabase();
        int cnt = localSQLiteDatabase.update(ORDER_TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    @Override
    public String getType(Uri uri) {

        return mimeTypes.get(uriMatcher.match(uri));
    }
}

