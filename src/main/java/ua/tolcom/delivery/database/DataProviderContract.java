package ua.tolcom.delivery.database;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.ArrayList;

import retrofit2.http.PATCH;

/**
 * Created by Andoliny on 10.10.2016.
 */

public class DataProviderContract{

    private DataProviderContract() {
    }

    public static final String AUTHORITY = "ua.tolcom.delivery";
    public static final String MAIN_URI_ROWS = "vnd.android.cursor.dir/vnd.";
    public static final String MAIN_URI_SINGLE = "vnd.android.cursor.dir/vnd.";

    //Набор строк
    public static final String MIME_TYPE_ROWS =
            MAIN_URI_ROWS + AUTHORITY;
    //Одна строка
    public static final String MIME_TYPE_SINGLE_ROW =
            MAIN_URI_SINGLE + AUTHORITY;


    // // Константы для БД
    // БД
    public static final String DATABASE_NAME = "Tolcom";
    public static final int DATABASE_VERSION = 1;

    public static final String SCHEME = "content";
    public static final Uri CONTENT_URI = Uri.parse(SCHEME + "://" + AUTHORITY);

    //Типы запросов
    public static final int URI_ORDER_QUERY = 1;
    public static final int URI_ORDER_BY_CLIENT_QUERY = 2;
    public static final int URI_ORDER_BY_ID_QUERY = 3;
    public static final int URI_ORDER_IN_ID_QUERY = 4;
    public static final int URI_ORDER_BY_STATE_QUERY = 5;

    public static final int URI_STATE_QUERY = 6;
    public static final int URI_STATE_BYID_QUERY = 7;

    public static final String ALIAS_JOINER = "_";

    //Константы для условий выборки
    public static final String BY_CLIENT = "/client/";
    public static final String BY_ID = "/id/";
    public static final String BY_STATE = "/state_id/";
    public static final String STATE_IN = "/state_in/";
    public static final String ID_IN = "/id_in/";

    public static String listId;


    public static final int INVALID_URI = -1;

    // Константы для построения SQLite таблиц во время инициализации
    public static final String TEXT_TYPE = "TEXT";
    public static final String PRIMARY_KEY_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL";
    public static final String INTEGER_TYPE = "INTEGER";
    public static final String FLOAT_TYPE = "REAL";


}