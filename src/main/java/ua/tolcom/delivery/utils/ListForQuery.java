package ua.tolcom.delivery.utils;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by Andoliny on 07.11.2016.
 */

public class ListForQuery<E> extends ArrayList {
    @Override
    public String toString() {
        E result;
        Iterator<E> iterator = listIterator();
        if (! iterator.hasNext())
            return " ";
        StringBuilder sb = new StringBuilder();
        for (;;) {
            E e = iterator.next();
            sb.append(e == this ? "(this ArrayList)" : e);
            if (! iterator.hasNext())
                return sb.toString();
            sb.append(',').append(' ');
        }
    }


}
