package ua.tolcom.delivery.utils;


import ua.tolcom.delivery.entities.DataStruct;

import static ua.tolcom.delivery.database.DataProviderContract.ALIAS_JOINER;

/**
 * Created by Andoliny on 03.11.2016.
 */

public class DataUtils {


    public static String[] getQualifiedColumns(String tableName, String [] columns) {
        String[] qualifiedColumns = new String[columns.length];
        for (int i = 0; i < columns.length; i++) {
            qualifiedColumns[i] = addPrefix(tableName, columns[i]);
        }
        return qualifiedColumns;
    }

    public static  String[] getTableColumns(DataStruct dataStruct){
        return dataStruct.getColumns();
    }

    public static String addPrefix(String tableName, String column) {
        return tableName + "." + column;
    }

    public static String[] createdCombinedProjection(String[] projectionFirst, String[] projectionSecond) {
        int firstLength = projectionFirst.length;
        int secondLength = projectionSecond.length;

        String combinedProjection[] = new String[firstLength + secondLength];
        for (int i = 0; i < firstLength; i++) {
            combinedProjection[i] = projectionFirst[i];
        }

        for (int i = 0; i < secondLength; i++) {
            combinedProjection[firstLength + i] = projectionSecond[i];
        }
        return combinedProjection;
    }

    public static String addAliasPrefix(String tableName, String column) {
        return tableName + ALIAS_JOINER + column;
    }

}
