package ua.tolcom.delivery.services;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.tolcom.delivery.api.ClientServerInterface;
import ua.tolcom.delivery.json.DownloadJson;
import ua.tolcom.delivery.json.OrderJson;
import ua.tolcom.delivery.security.AndroidAuthData;

import static ua.tolcom.delivery.consts.DeliveryConsts.*;
import static ua.tolcom.delivery.entities.Order.*;
import static ua.tolcom.delivery.security.SecurityConsts.*;

/**
 * Created by Andoliny on 19.09.2016.
 */
public class DownloadService extends Service{

    public DownloadService() {
    }

    @Override
    public void onCreate() {
        Log.d(TAG_LOG, "DownloadService onCreate");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG_LOG, "DownloadService onstartCommand");

        downloadFromServer();

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void downloadFromServer() {
        SharedPreferences preferences = getSharedPreferences(PREFS, MODE_PRIVATE);

        final Intent intent = new Intent(BROADCAST_WORKSERVICE);
        intent.putExtra(PARAM_STATUS, STATUS_START_DOWNLOAD);
        intent.putExtra(PARAM_TASK, DOWNLOADING_TASK);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);


        ClientServerInterface service = ApiFactory.getClientServerInterface(preferences);
        Call<DownloadJson> callServerInfo = service.getDataBody(new AndroidAuthData(LOGIN, PASSWORD));

        callServerInfo.enqueue(new Callback<DownloadJson>() {
            @Override
            public void onResponse(Call<DownloadJson> call, Response<DownloadJson> response) {
                if(response.isSuccessful()){
                    DownloadJson downloadJson = response.body();
                    if(downloadJson.getErrorMessage().equals("")){
                        Log.d(TAG_LOG, "DownloadService: response isSuccsess: " + response.isSuccessful());

                        Log.d(TAG_LOG, "DownloadService: db insert start ");
                        Log.d(TAG_LOG, downloadJson.toString());
                        insertIntoDB(downloadJson.getOrdersJson());
                        Log.d(TAG_LOG, "DownloadService: db insert stop ");
                        intent.putExtra(PARAM_STATUS, STATUS_FINISH_DOWNLOAD);
                        intent.putExtra(PARAM_TASK, DOWNLOADING_TASK);
                        intent.putExtra(PARAM_DOWNLOAD_QUANTITY, downloadJson.getOrdersJson().size());
                        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                        sendBroadcast(intent);
                    }
                    else {
                        intent.putExtra(PARAM_STATUS, STATUS_SERVER_QUERY_ERROR);
                        intent.putExtra(PARAM_TASK, DOWNLOADING_TASK);
                        intent.putExtra(PARAM_ERROR, downloadJson.getErrorMessage());
                        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                        sendBroadcast(intent);
                    }
                }
                else {
                    Log.d(TAG_LOG, "DownloadService: response fail");
                    System.out.println("Error");
                }
                stop();
            }

            @Override
            public void onFailure(Call<DownloadJson> call, Throwable t) {
                Log.d(TAG_LOG, "DownloadService: onFailure when call api: "  +t);
                stop();

            }

        });
        Log.d(TAG_LOG, "DownloadService: return");
    }

    private void insertIntoDB(List<OrderJson> downloadJsonList) {
        Log.d(TAG_LOG, downloadJsonList.toString());
        for(OrderJson downloadJson : downloadJsonList){
            ContentValues values = new ContentValues();
            values.put(ORDER_ID_ORDER, downloadJson.getIdOrder());
            values.put(ORDER_CLIENT_ID, downloadJson.getIdClient());
            values.put(ORDER_NAME, downloadJson.getProductName());
            values.put(ORDER_PRICE, downloadJson.getPrice());
            values.put(ORDER_TOTAL_PRICE, downloadJson.getSum());
            values.put(ORDER_QUANTITY, downloadJson.getCount());
            values.put(ORDER_CLIENT_NAME, downloadJson.getClientName());
            values.put(ORDER_CLIENT_PHONE, downloadJson.getPhoneNamber());
            values.put(ORDER_DATE, downloadJson.getOrderDate());
            values.put(ORDER_DELIVERY_DATE, downloadJson.getDeliveryDate());
            values.put(ORDER_STATE, STATE_NEW);
            getContentResolver().insert(ORDER_TABLE_CONTENTURI, values);
            Log.d(TAG_LOG, values.toString());
        }
    }

    private void stop(){
        Log.d(TAG_LOG, "DownloadService: stopped ");
        stopSelf();
    }


    @Override
    public void onDestroy() {
        Log.d(TAG_LOG, "DownloadService: onDestroy");
        stopSelf();
        super.onDestroy();
    }
}


