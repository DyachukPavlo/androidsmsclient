package ua.tolcom.delivery.services;

import android.content.SharedPreferences;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.tolcom.delivery.api.ClientServerInterface;
import ua.tolcom.delivery.json.ClientAnswerJson;

import static ua.tolcom.delivery.consts.DeliveryConsts.PREFS;
import static ua.tolcom.delivery.consts.DeliveryConsts.TAG_LOG;

/**
 * Created by Andoliny on 09.11.2016.
 */

public class Uploader {

    public void uploadToServer(ClientAnswerJson clientAnswerJson, SharedPreferences sharedPreferences){

        ClientServerInterface service = ApiFactory.getClientServerInterface(sharedPreferences);

        Call<Boolean> callClientInfo = service.updateStateOnServer(clientAnswerJson);
        callClientInfo.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.isSuccessful()){
                    boolean answer = response.body();
                    Log.d(TAG_LOG, "Uploader: response success");
                    if(!answer){
                        Log.d(TAG_LOG, "Uploader: Error on server");
                    }
                }
                else {
                    Log.d(TAG_LOG, "Uploader: response fail");
                    System.out.println("Error");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.d(TAG_LOG, "Uploader: onFailure when call api: "  +t);
            }
        });
    }

}
