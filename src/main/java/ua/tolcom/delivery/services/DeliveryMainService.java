package ua.tolcom.delivery.services;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ua.tolcom.delivery.consts.DeliveryConsts.*;


public class DeliveryMainService extends Service {

    Timer myTimer;

    private ExecutorService es;
    private BroadcastReceiver br;
    private int quantityDownload;
    private int quantitySent;
    private int status;
    private int task;
    private String error;
    private Intent intDataDownloadService;
    private Intent intSmsSendServ;
    private final Intent mainServiceBroadcastIntent = new Intent(BROADCAST_MAINSERVICE);

    public DeliveryMainService() {
        super();
    }

    public void onCreate() {
        super.onCreate();
        Log.d(TAG_LOG, "DeliveryMainService onCreate");

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                status = intent.getIntExtra(PARAM_STATUS, 0);
                task = intent.getIntExtra(PARAM_TASK, 0);
                error = intent.getStringExtra(PARAM_ERROR);


                switch (task) {
                    case DOWNLOADING_TASK:
                        if (status == STATUS_START_DOWNLOAD) {
                            mainServiceBroadcastIntent.putExtra(PARAM_STATUS, status);
                        } else {
                            if (status == STATUS_FINISH_DOWNLOAD) {
                                quantityDownload = intent.getIntExtra(PARAM_DOWNLOAD_QUANTITY, 0);
                                mainServiceBroadcastIntent.putExtra(PARAM_STATUS, status);
                                mainServiceBroadcastIntent.putExtra(PARAM_DOWNLOAD_QUANTITY, quantityDownload);
                            } else {
                                if (status == STATUS_SERVER_QUERY_ERROR) {
                                    mainServiceBroadcastIntent.putExtra(PARAM_STATUS, status);
                                    mainServiceBroadcastIntent.putExtra(PARAM_ERROR, error);
                                }
                            }
                        }
                        break;
                    case SENDING_TASK:
                        if (status == STATUS_START_SENDING) {
                            mainServiceBroadcastIntent.putExtra(PARAM_STATUS, status);
                        } else {
                            if (status == STATUS_FINISH_SENDING) {
                                quantitySent = intent.getIntExtra(PARAM_SENT_QUANTITY, 0);
                                mainServiceBroadcastIntent.putExtra(PARAM_STATUS, status);
                                mainServiceBroadcastIntent.putExtra(PARAM_SENT_QUANTITY, quantitySent);
                                Log.d(TAG_LOG, "quantitysent = " + quantitySent);
                            } else {
                                if (status == STATUS_SERVER_QUERY_ERROR) {
                                    mainServiceBroadcastIntent.putExtra(PARAM_STATUS, status);
                                    mainServiceBroadcastIntent.putExtra(PARAM_ERROR, error);
                                }
                            }

                        }
                        break;
                }

                mainServiceBroadcastIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                sendBroadcast(mainServiceBroadcastIntent);
            }
        }

        ;
        IntentFilter intentFilterWorkService = new IntentFilter(BROADCAST_WORKSERVICE);

        registerReceiver(br, intentFilterWorkService);

        es = Executors.newFixedThreadPool(1);
        Notification.Builder builder = new Notification.Builder(this).setSmallIcon(ua.tolcom.delivery.R.mipmap.ic_launcher);
        Notification notification;
        if (Build.VERSION.SDK_INT < 16)

        {
            notification = builder.getNotification();
        } else

        {
            notification = builder.build();
        }

        startForeground(77, notification);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG_LOG, "DeliveryMainService onstartCommand");
        RunnableTask runnableTask = new RunnableTask(startId);
        es.execute(runnableTask);

        return super.onStartCommand(intent, flags, startId);
    }

    public class RunnableTask implements Runnable {
        private int startId;
        SharedPreferences preferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        Integer interval = preferences.getInt(PREF_INTERVAL, DEFAULT_INTERVAL_MINUTES);

        public RunnableTask(int startId) {
            this.startId = startId;
        }

        @Override
        public void run() {
            myTimer = new Timer();
            myTimer.schedule(new TimerTask() {
                @Override
                public void run() {

                    Log.w(TAG_LOG, "DMS: download");
                    intDataDownloadService = new Intent(DeliveryMainService.this, DownloadService.class);
                    startService(intDataDownloadService);
                    stopService(intDataDownloadService);
                    Log.w(TAG_LOG, "DMS: send");
                    intSmsSendServ = new Intent(DeliveryMainService.this, SenderService.class);
                    startService(intSmsSendServ);
                    stopService(intSmsSendServ);

                };
            }, FIRST_RUN, interval * INTERVAL_MULTIPLIER);

/*
            myTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Intent intSmsSendServ = new Intent(DeliveryMainService.this, SenderService.class);
                    startService(intSmsSendServ);

                    intent.putExtra(PARAM_STATUS, STATUS_FINISH);
                    intent.putExtra(PARAM_RESULT, result);


                    sendBroadcast(intent);
                    stopService(intSmsSendServ);
                };
            }, FIRST_RUN, INTERVAL);

*/

        }
    }


    @Override
    public void onDestroy() {

        if (myTimer != null) {
            myTimer.cancel();
            unregisterReceiver(br);
            stopService(intDataDownloadService);
            stopService(intSmsSendServ);
            //   Log.d(TAG_LOG, "Timer cancel");
        }
        //    Log.d(TAG_LOG, "DeliveryMainService onDestroy");
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        //   Log.d(TAG_LOG, "DeliveryMainService onBind");
        return new Binder();
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        //    Log.d(TAG_LOG, "DeliveryMainService onRebind");
    }

    public boolean onUnbind(Intent intent) {
        //    Log.d(TAG_LOG, "DeliveryMainService onUnbind");
        return true;
    }

}
