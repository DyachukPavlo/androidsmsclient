package ua.tolcom.delivery.services;

import android.Manifest;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ua.tolcom.delivery.activities.MainActivity;
import ua.tolcom.delivery.entities.Order;
import ua.tolcom.delivery.entities.OrderState;
import ua.tolcom.delivery.entities.SmsInfo;
import ua.tolcom.delivery.json.ClientAnswerJson;
import ua.tolcom.delivery.security.AndroidAuthData;
import ua.tolcom.delivery.utils.ListForQuery;

import static ua.tolcom.delivery.consts.DeliveryConsts.*;
import static ua.tolcom.delivery.database.DataProviderContract.*;
import static ua.tolcom.delivery.security.SecurityConsts.*;
import static ua.tolcom.delivery.utils.DataUtils.*;
import static ua.tolcom.delivery.entities.Order.*;
import static ua.tolcom.delivery.entities.OrderState.*;

/**
 * Created by Andoliny on 26.10.2016.
 */

public class SenderService extends Service {

    final Intent intentRequest = new Intent(BROADCAST_WORKSERVICE);
    private final String[] orderProjection = getTableColumns(new Order());
    private final String[] orderStateProjection = getTableColumns(new OrderState());

    private List<SmsInfo> smsInfoList;
    List<Integer> listId;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG_LOG, "SenderService: oncreate");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        selectSmsData();
        sendSms();
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendSms() {
        SharedPreferences preferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        Uploader uploader;
        List<Integer> idOrdersJsonList;
        if(smsInfoList != null){
            intentRequest.putExtra(PARAM_STATUS, STATUS_START_SENDING);
            intentRequest.putExtra(PARAM_TASK, SENDING_TASK);
            intentRequest.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            sendBroadcast(intentRequest);

            Log.d(TAG_LOG, "SenderService: sent mess start");
            idOrdersJsonList = new ArrayList<>();

            ObjectMapper oMapper = new ObjectMapper();

            for (SmsInfo smsInfo : smsInfoList){
                SmsManager sms = SmsManager.getDefault();
                String message = preferences.getString(PREF_MESSAGE_TEXT, "");

                Map<String, String> values = oMapper.convertValue(smsInfo, Map.class);
                StringBuffer sb = new StringBuffer();
                Matcher m = Pattern.compile("\\{(\\w+)\\}").matcher(message);
                while (m.find()) {
                    m.appendReplacement(sb, String.valueOf(values.get(m.group(1))));
                }
                m.appendTail(sb);
                String result = sb.toString();

                try{
                    ArrayList<String> smsTexts = sms.divideMessage(result);
                    Log.d(TAG_LOG, smsTexts.toString());
                    sms.sendMultipartTextMessage(/*"5554"*/smsInfo.getPhoneNamber(), null, smsTexts, null, null);
                    idOrdersJsonList.add(smsInfo.getIdOrder());
                }
                catch (Exception e){
                    Log.e(TAG_LOG, "There was sending exception on: " + smsInfo.toString() + ", " + e);
                    smsInfoList.remove(smsInfo);
                }
            }
            uploader = new Uploader();
            if(idOrdersJsonList != null){
                uploader.uploadToServer(new ClientAnswerJson(new AndroidAuthData(LOGIN, PASSWORD),idOrdersJsonList), preferences);
            }
            updateStatusSms();
            Log.d(TAG_LOG, "SenderService: sent mess end = " + smsInfoList.size());
            intentRequest.putExtra(PARAM_STATUS, STATUS_FINISH_SENDING);
            intentRequest.putExtra(PARAM_TASK, SENDING_TASK);
            intentRequest.putExtra(PARAM_SENT_QUANTITY, smsInfoList.size());
            intentRequest.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            sendBroadcast(intentRequest);
        }
    }

    private void updateStatusSms(){
        if(smsInfoList != null) {
            ContentValues values = new ContentValues();
            values = new ContentValues();
            values.put(ORDER_STATE, STATE_COMPLETE);
            getContentResolver().update(Uri.parse(ORDER_TABLE_CONTENTURI + ID_IN + listId.toString()),
                    values,
                    null,
                    null);
        }
    }

    private void selectSmsData(){
        SmsInfo smsInfo;
        Log.d(TAG_LOG, "SenderService: select smsData start");
        String[] projection = createdCombinedProjection(getQualifiedColumns(ORDER_TABLE_NAME, orderProjection),getQualifiedColumns(STATE_TABLE_NAME,orderStateProjection));
        listId = new ListForQuery<Integer>();

        Cursor cursor = null;
        cursor = getContentResolver().query(
               // ORDER_TABLE_CONTENTURI,
                Uri.parse(ORDER_TABLE_CONTENTURI + BY_STATE + STATE_NEW),
                projection,
                null,
                null,
                null);
        if (null != cursor && cursor.moveToFirst()) {
            smsInfoList = new ArrayList<>();
            do{
                smsInfo = new SmsInfo();
                smsInfo.setIdOrder(cursor.getInt(cursor.getColumnIndex(ORDER_ID_ORDER)));
                smsInfo.setProductName(cursor.getString(cursor.getColumnIndex(ORDER_NAME)));
                smsInfo.setTotalPrice(cursor.getDouble(cursor.getColumnIndex(ORDER_TOTAL_PRICE)));
                smsInfo.setClientName(cursor.getString(cursor.getColumnIndex(ORDER_CLIENT_NAME)));
                smsInfo.setOrderDate(cursor.getString(cursor.getColumnIndex(ORDER_DATE)));
                smsInfo.setDeliveryDate(cursor.getString(cursor.getColumnIndex(ORDER_DELIVERY_DATE)));
                smsInfo.setClientName(cursor.getString(cursor.getColumnIndex(ORDER_CLIENT_NAME)));
                smsInfo.setPrice(cursor.getDouble(cursor.getColumnIndex(ORDER_PRICE)));
                smsInfo.setPhoneNamber(cursor.getString(cursor.getColumnIndex(ORDER_CLIENT_PHONE)));
                smsInfo.setCount(cursor.getInt(cursor.getColumnIndex(ORDER_ID)));

                listId.add(smsInfo.getIdOrder());
                smsInfoList.add(smsInfo);
                Log.w(TAG_LOG, smsInfo.toString());
            } while (cursor.moveToNext());
        }
        cursor.close();
        Log.d(TAG_LOG, "SenderService: select smsData finish");
        Log.d(TAG_LOG, listId.toString());
    }
}
