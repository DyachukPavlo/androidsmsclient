package ua.tolcom.delivery.services;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import ua.tolcom.delivery.api.ClientServerInterface;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static android.content.Context.MODE_PRIVATE;
import static ua.tolcom.delivery.consts.DeliveryConsts.*;

public class ApiFactory {
    @NonNull
    public static ClientServerInterface getClientServerInterface(SharedPreferences preferences) {
        String apiEndpoint = preferences.getString(PREF_SERVER_ADDR, "");
        return getRetrofit(apiEndpoint).create(ClientServerInterface.class);
    }

    @NonNull
    private static Retrofit getRetrofit(String endpoint) {

        return new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }
}
