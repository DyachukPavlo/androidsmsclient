
package ua.tolcom.delivery.json;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ordersJson", "errorMessage"
})
public class DownloadJson {

    @JsonProperty("ordersJson")
    private List<OrderJson> ordersJson = new ArrayList<>();

    @JsonProperty("errorMessage")
    private String errorMessage;

    /**
     * No args constructor for use in serialization
     *
     */
    public DownloadJson() {
    }


    /**
     *
     * @param ordersJson
     */
    public DownloadJson(List<OrderJson> ordersJson, String errorMessage) {
        this.ordersJson = ordersJson;
        this.errorMessage = errorMessage;
    }

    /**
     *
     * @return
     *     The ordersJson
     */
    @JsonProperty("ordersJson")
    public List<OrderJson> getOrdersJson() {
        return ordersJson;
    }

    /**
     *
     * @param ordersJson
     *     The ordersJson
     */
    @JsonProperty("ordersJson")
    public void setOrdersJson(List<OrderJson> ordersJson) {
        this.ordersJson = ordersJson;
    }

    @JsonProperty("errorMessage")
    public String getErrorMessage() {
        return errorMessage;
    }

    @JsonProperty("errorMessage")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "DownloadJson{" +
                "ordersJson=" + ordersJson.toString() +
                '}';
    }
}
