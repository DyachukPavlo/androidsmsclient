package ua.tolcom.delivery.json;

/**
 * Created by Andoliny on 09.11.2016.
 */


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderJson {

    @JsonProperty("idOrder")
    private int idOrder;
    @JsonProperty("orderDate")
    private String orderDate;
    @JsonProperty("deliveryDate")
    private String deliveryDate;
    @JsonProperty("idClient")
    private int idClient;
    @JsonProperty("clientName")
    private String clientName;
    @JsonProperty("productName")
    private String productName;
    @JsonProperty("price")
    private double price;
    @JsonProperty("count")
    private int count;
    @JsonProperty("sum")
    private double sum;
    @JsonProperty("phoneNamber")
    private String phoneNamber;
    @JsonProperty("state")
    private String state;

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderJson() {
    }

    public OrderJson(int idOrder, String orderDate, String deliveryDate, int idClient, String clientName, String productName, double price, int count, double sum, String phoneNamber, String state) {
        this.idOrder = idOrder;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
        this.idClient = idClient;
        this.clientName = clientName;
        this.productName = productName;
        this.price = price;
        this.count = count;
        this.sum = sum;
        this.phoneNamber = phoneNamber;
        this.state = state;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public String getPhoneNamber() {
        return phoneNamber;
    }

    public void setPhoneNamber(String phoneNamber) {
        this.phoneNamber = phoneNamber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "OrderJson{" +
                "idOrder=" + idOrder +
                ", orderDate='" + orderDate + '\'' +
                ", deliveryDate='" + deliveryDate + '\'' +
                ", idClient=" + idClient +
                ", clientName='" + clientName + '\'' +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", sum=" + sum +
                ", phoneNamber='" + phoneNamber + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
