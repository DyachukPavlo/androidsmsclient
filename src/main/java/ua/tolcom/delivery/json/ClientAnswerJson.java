package ua.tolcom.delivery.json;

import java.util.List;

import ua.tolcom.delivery.security.AndroidAuthData;

/**
 * Created by Andoliny on 10.11.2016.
 */

public class ClientAnswerJson {
    private AndroidAuthData androidAuthData;
    private List<Integer> idOrdersList;

    public ClientAnswerJson() {
    }

    public ClientAnswerJson(AndroidAuthData androidAuthData, List<Integer> idOrdersList) {
        this.androidAuthData = androidAuthData;
        this.idOrdersList = idOrdersList;
    }

    public AndroidAuthData getAndroidAuthData() {
        return androidAuthData;
    }

    public void setAndroidAuthData(AndroidAuthData androidAuthData) {
        this.androidAuthData = androidAuthData;
    }

    public List<Integer> getIdOrdersList() {
        return idOrdersList;
    }

    public void setIdOrdersList(List<Integer> idOrdersList) {
        this.idOrdersList = idOrdersList;
    }

    @Override
    public String toString() {
        return "ClientAnswerJson{" +
                "androidAuthData=" + androidAuthData +
                ", idOrdersList=" + idOrdersList +
                '}';
    }
}
