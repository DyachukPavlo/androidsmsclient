package ua.tolcom.delivery.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Andoliny on 09.11.2016.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "idOrder"
})

public class CompleteOrdersJson {
    @JsonProperty("idOrder")
    private int idOrder;

    public CompleteOrdersJson(int idOrder) {
        this.idOrder = idOrder;
    }

    public CompleteOrdersJson() {
    }

    @JsonProperty("idOrder")
    public int getIdOrder() {
        return idOrder;
    }

    @JsonProperty("idOrder")
    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    @Override
    public String toString() {
        return "CompleteOrdersJson{" +
                "idOrder=" + idOrder +
                '}';
    }
}
