package ua.tolcom.delivery.api;

import retrofit2.http.Body;
import retrofit2.http.POST;
import ua.tolcom.delivery.json.ClientAnswerJson;
import ua.tolcom.delivery.json.DownloadJson;

import retrofit2.Call;
import ua.tolcom.delivery.security.AndroidAuthData;

/**
 * Created by Andoliny on 09.10.2016.
 */
public interface ClientServerInterface {
    @POST("/sms")
    Call<DownloadJson> getDataBody(@Body AndroidAuthData androidAuthData);


    @POST("/smsanswer")
    Call<Boolean> updateStateOnServer(@Body ClientAnswerJson clientAnswerJson);
}
