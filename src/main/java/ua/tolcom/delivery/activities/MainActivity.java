package ua.tolcom.delivery.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.PersistableBundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ua.tolcom.delivery.R;
import ua.tolcom.delivery.entities.Order;
import ua.tolcom.delivery.entities.OrderState;
import ua.tolcom.delivery.entities.SmsInfo;
import ua.tolcom.delivery.services.DeliveryMainService;

import static ua.tolcom.delivery.consts.DeliveryConsts.*;
import static ua.tolcom.delivery.utils.DataUtils.*;

public class MainActivity extends AppCompatActivity  {

    private boolean bound = false;
    private ServiceConnection sConn;
    private Intent intent;
    private SharedPreferences settings;

    private TextView tvDownloadingState;
    private TextView tvSendingState;
    private Button btnStartSender;
    private Button btnStopSender;
    private Button btnPreferences;
    private int downloadQuantity;
    private int sentQuantity;
    private TextView lblAddress;

    private final String[] orderProjection = getTableColumns(new Order());
    private final String[] orderStateProjection = getTableColumns(new OrderState());
    private List<SmsInfo> smsInfoList;

    private BroadcastReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(ua.tolcom.delivery.R.layout.activity_main);
        settings = getSharedPreferences(PREFS, MODE_PRIVATE);

        tvDownloadingState = (TextView) findViewById(ua.tolcom.delivery.R.id.tvDownloadingState);
        tvSendingState = (TextView) findViewById(ua.tolcom.delivery.R.id.tvSendingState);

        tvDownloadingState.setText("No tasks");
        tvSendingState.setText("No tasks");

        btnStartSender = (Button)findViewById(R.id.btnStartSender);
        btnStopSender = (Button)findViewById(R.id.btnStopSender);
        btnPreferences = (Button)findViewById(R.id.btnPreferences);

        lblAddress = (TextView)findViewById(R.id.lblAddress);
        lblAddress.setText("Server IP: " + settings.getString(PREF_SERVER_ADDR,""));

        btnStopSender.setEnabled(false);



        sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d(TAG_LOG, "MainActivity: onServiceConnected");
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG_LOG, "MainActivity: onServiceDisconnected");
                bound = false;
            }
        };
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(PARAM_STATUS,0);
                String error = intent.getStringExtra(PARAM_ERROR);



                Log.d(TAG_LOG, "MainActivity: onReceive status = " +status);

                if (status == STATUS_START_DOWNLOAD){
                    tvDownloadingState.setText("Downloading");
                }

                if (status == STATUS_FINISH_DOWNLOAD){
                    downloadQuantity += intent.getIntExtra(PARAM_DOWNLOAD_QUANTITY,0);
                    tvDownloadingState.setText("Downloaded: " + downloadQuantity +" orders");
                }

                if (status == STATUS_SERVER_QUERY_ERROR){
                    tvDownloadingState.setText("Downloaded: " + downloadQuantity +" orders");
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }

                if (status == STATUS_START_SENDING){
                    tvSendingState.setText("Sending");
                }

                if (status == STATUS_FINISH_SENDING){
                    sentQuantity += intent.getIntExtra(PARAM_SENT_QUANTITY, 0);
                    tvSendingState.setText("Sent: " + sentQuantity + " sms");
                }
            }
        };

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        1);
            }
        } else {
            // Permission has already been granted
            Log.d(TAG_LOG, "Permission has already been granted");
        }

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 2);
        } else {
            Log.d(TAG_LOG, "Permission has not been granted");
        }


        IntentFilter intentFilterMain = new IntentFilter(BROADCAST_MAINSERVICE);
        registerReceiver(br,intentFilterMain);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 2:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                }
                break;

            default:
                break;

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);
        if (!bound) return;
        unbindService(sConn);
        bound = false;
    }

    public void onClickStart(View view){
        Log.d(TAG_LOG,"MainActivity: onClickStart");

        intent = new Intent(this, DeliveryMainService.class);
        startService(intent);

        bindService(intent, sConn, BIND_AUTO_CREATE);

        btnStopSender.setEnabled(true);
        btnStartSender.setEnabled(false);

    }

    public void onClickStop(View v) {
        if (!bound) return;
        unbindService(sConn);
        bound = false;
        stopService(intent);
        btnStartSender.setEnabled(true);
        btnStopSender.setEnabled(false);
    }

    public void onClickPreferences(View v){
        Intent intent = new Intent(MainActivity.this, PreferencesActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        Log.d(TAG_LOG, settings.getString(PREF_SERVER_ADDR,""));
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        lblAddress.setText("Server IP: " + settings.getString(PREF_SERVER_ADDR,""));
    }
}
