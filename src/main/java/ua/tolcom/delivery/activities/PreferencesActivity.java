package ua.tolcom.delivery.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.travijuu.numberpicker.library.NumberPicker;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ua.tolcom.delivery.R;
import ua.tolcom.delivery.entities.SmsInfo;

import static ua.tolcom.delivery.consts.DeliveryConsts.DEFAULT_ADDRESS;
import static ua.tolcom.delivery.consts.DeliveryConsts.DEFAULT_INTERVAL_MINUTES;
import static ua.tolcom.delivery.consts.DeliveryConsts.DEFAULT_MESSAGE;
import static ua.tolcom.delivery.consts.DeliveryConsts.PREFS;
import static ua.tolcom.delivery.consts.DeliveryConsts.PREF_INTERVAL;
import static ua.tolcom.delivery.consts.DeliveryConsts.PREF_MESSAGE_TEXT;
import static ua.tolcom.delivery.consts.DeliveryConsts.PREF_SERVER_ADDR;
import static ua.tolcom.delivery.consts.DeliveryConsts.TAG_LOG;

public class PreferencesActivity extends AppCompatActivity {

    private SharedPreferences settings;
    private EditText serverIp;
    private EditText messageText;
    private NumberPicker numberPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);

        serverIp = (EditText) findViewById(R.id.serverAddr);
        messageText = (EditText) findViewById(R.id.messageText);
        numberPicker = (NumberPicker) findViewById(R.id.numberpicker);

        settings = getSharedPreferences(PREFS, MODE_PRIVATE);
        String ip = settings.getString(PREF_SERVER_ADDR,DEFAULT_ADDRESS);

        numberPicker.setMin(1);
        numberPicker.setMax(999);

        String message = settings.getString(PREF_MESSAGE_TEXT, DEFAULT_MESSAGE);
        Integer interval = settings.getInt(PREF_INTERVAL, DEFAULT_INTERVAL_MINUTES);

        serverIp.setText(ip);
        messageText.setText(message);
        numberPicker.setValue(interval);

    }

    public void saveChanges(View view) {

        String ip = serverIp.getText().toString();
        String message = messageText.getText().toString();
        Integer interval = numberPicker.getValue();

        SharedPreferences.Editor prefEditor = settings.edit();
        prefEditor.putString(PREF_SERVER_ADDR, ip);
        prefEditor.putString(PREF_MESSAGE_TEXT, message);
        prefEditor.putInt(PREF_INTERVAL, interval);
        prefEditor.apply();
    }
}